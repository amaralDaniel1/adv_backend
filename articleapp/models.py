# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Article(models.Model):
    # publisher = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    body = models.TextField(max_length=2000)
    pub_date = models.DateTimeField('date published')
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.title