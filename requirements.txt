dj-database-url==0.4.2
Django==1.11.3
django-admin-tools==0.8.1
gunicorn==19.7.1
psycopg2==2.7.1
pytz==2017.2
whitenoise==3.3.0
