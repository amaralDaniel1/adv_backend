#Carlos Soares Advogados - Article Publishing tool

it's running on Heroku. 

##How to run? 

* install [pip](https://pip.pypa.io/en/stable/installing/)
* install dependencies
> pip install -r requirements.txt

* run dev server (**not suited for production**)
> python manage.py runserver


anything else, refer to @amaralDaniel1 at danielamaral@jitt.travel

